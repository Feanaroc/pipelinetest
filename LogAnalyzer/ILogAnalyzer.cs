﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogAnalyzerLibrary
{
    public interface ILogAnalyzer
    {
        bool LastFileNameWasValid { get; set; }

        bool IsValidLogFileName(string fileName);
        bool IsValidLogFileName_Stub(string fileName);
    }
}
